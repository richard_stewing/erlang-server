-module(hello_world).
-export([start/0,service/3, answerRequest/1, service122/3]).

start() ->
  inets:start(),
  inets:start(httpd, [
  {modules, [
   mod_alias, 
   mod_auth, 
   mod_esi, 
   mod_actions, 
   mod_cgi, 
   mod_dir, 
   mod_get, 
   mod_head, 
   mod_log, 
   mod_disk_log
  ]},
  {port,8081},
  {server_name,"hello_world"},
  {server_root,"log"},
  {document_root,"www"},
  {erl_script_alias, {"/erl", [hello_world]}},
  {error_log, "error.log"},
  {security_log, "security.log"},
  {transfer_log, "transfer.log"},
  {mime_types,[
   {"html","text/html"},
   {"css","text/css"},
   {"js","application/x-javascript"}
  ]}
 ]).
 
service(SessionID, _Env, _Input) ->
  spawn(?MODULE, answerRequest, [SessionID]),
  io:format("The seasion id ~p~n",[SessionID]).

service122(SessionID, _Env, Input) ->
  mod_esi:deliver(SessionID, [
    "Content-Type: text/html\r\n\r\n", 
    "test"]),
  io:format("The seasion id ~p with data ~p~n",[SessionID, Input]).

answerRequest(SessionID)->
  {ok, PageBi} = file:read_file('page.html'),
  PageStr = binary_to_list(PageBi),
  mod_esi:deliver(SessionID, [
    "Content-Type: text/html\r\n\r\n", 
    PageStr]).
